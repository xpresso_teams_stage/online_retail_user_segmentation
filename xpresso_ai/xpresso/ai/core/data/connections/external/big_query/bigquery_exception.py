"""Module for creating user-defined exceptions"""


class Error(Exception):
    """Base Class for all exceptions"""
    pass


class BigQueryClientException(Error):
    """Raised when Client connection throws an exception"""
    pass


class BigQueryImportFail(Error):
    """Raised when Query job fails and a table is not imported"""
    pass
